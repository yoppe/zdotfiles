################################################################################
##    Changing Directories                                                    ##
################################################################################
setopt CHASE_LINKS
# Resolve symbolic links to their true values when changing directory. This also has the effect of CHASE_DOTS, i.e. a ‘..’ path segment will be treated as referring to the physical parent, even if the preceding path segment is a symbolic link.

setopt PUSHD_IGNORE_DUPS
# Don’t push multiple copies of the same directory onto the directory stack.


################################################################################
##    Completions                                                             ##
################################################################################
setopt AUTO_LIST
# Automatically list choices on an ambiguous completion.

setopt AUTO_MENU
# Automatically use menu completion after the second consecutive request for completion, for example by pressing the tab key repeatedly. This option is overridden by MENU_COMPLETE.

setopt AUTO_NAME_DIRS
# Any parameter that is set to the absolute name of a directory immediately becomes a name for that directory, that will be used by the ‘%~’ and related prompt sequences, and will be available when completion is performed on a word starting with ‘~’. (Otherwise, the parameter must be used in the form ‘~param’ first.)

setopt AUTO_PARAM_KEYS
# If a parameter name was completed and a following character (normally a space) automatically inserted, and the next character typed is one of those that have to come directly after the name (like ‘}’, ‘:’, etc.), the automatically added character is deleted, so that the character typed comes immediately after the parameter name. Completion in a brace expansion is affected similarly: the added character is a ‘,’, which will be removed if ‘}’ is typed next.

setopt AUTO_PARAM_SLASH
# If a parameter is completed whose content is the name of a directory, then add a trailing slash instead of a space.

setopt AUTO_REMOVE_SLASH
# When the last character resulting from a completion is a slash and the next character typed is a word delimiter, a slash, or a character that ends a command (such as a semicolon or an ampersand), remove the slash.

setopt COMPLETE_ALIASES
# Prevents aliases on the command line from being internally substituted before completion is attempted. The effect is to make the alias a distinct command for completion purposes.

setopt LIST_PACKED
# Try to make the completion list smaller (occupying less lines) by printing the matches in columns with different widths.


################################################################################
##    Expansion and Globbing                                                  ##
################################################################################
setopt EQUALS
# Perform = filename expansion. (See Filename Expansion.)

setopt GLOB
# Perform filename generation (globbing). (See Filename Generation.)

setopt GLOB_STAR_SHORT
# When this option is set and the default zsh-style globbing is in effect, the pattern ‘**/*’ can be abbreviated to ‘**’ and the pattern ‘***/*’ can be abbreviated to ***. Hence ‘**.c’ finds a file ending in .c in any subdirectory, and ‘***.c’ does the same while also following symbolic links. A / immediately after the ‘**’ or ‘***’ forces the pattern to be treated as the unabbreviated form.

setopt MAGIC_EQUAL_SUBST
# All unquoted arguments of the form ‘anything=expression’ appearing after the command name have filename expansion (that is, where expression has a leading ‘~’ or ‘=’) performed on expression as if it were a parameter assignment. The argument is not otherwise treated specially; it is passed to the command as a single argument, and not used as an actual parameter assignment. For example, in echo foo=~/bar:~/rod, both occurrences of ~ would be replaced. Note that this happens anyway with typeset and similar statements.

# This option respects the setting of the KSH_TYPESET option. In other words, if both options are in effect, arguments looking like assignments will not undergo word splitting.

setopt MARK_DIRS
# Append a trailing ‘/’ to all directory names resulting from filename generation (globbing).

setopt NUMERIC_GLOB_SORT
# If numeric filenames are matched by a filename generation pattern, sort the filenames numerically rather than lexicographically.


################################################################################
##    History                                                                 ##
################################################################################
setopt BANG_HIST
# Perform textual history expansion, csh-style, treating the character ‘!’ specially.

setopt HIST_IGNORE_ALL_DUPS
# If a new command line being added to the history list duplicates an older one, the older command is removed from the list (even if it is not the previous event).

setopt HIST_IGNORE_DUPS
# Do not enter command lines into the history list if they are duplicates of the previous event.

setopt HIST_IGNORE_SPACE
# Remove command lines from the history list when the first character on the line is a space, or when one of the expanded aliases contains a leading space. Only normal aliases (not global or suffix aliases) have this behaviour. Note that the command lingers in the internal history until the next command is entered before it vanishes, allowing you to briefly reuse or edit the line. If you want to make it vanish right away without entering another command, type a space and press return.

setopt HIST_NO_FUNCTIONS
# Remove function definitions from the history list. Note that the function lingers in the internal history until the next command is entered before it vanishes, allowing you to briefly reuse or edit the definition.

setopt HIST_REDUCE_BLANKS
# Remove superfluous blanks from each command line being added to the history list.

setopt HIST_SAVE_NO_DUPS
# When writing out the history file, older commands that duplicate newer ones are omitted.

setopt HIST_VERIFY
# Whenever the user enters a line with history expansion, don’t execute the line directly; instead, perform history expansion and reload the line into the editing buffer.

setopt SHARE_HISTORY
# This option both imports new commands from the history file, and also causes your typed commands to be appended to the history file (the latter is like specifying INC_APPEND_HISTORY, which should be turned off if this option is in effect). The history lines are also output with timestamps ala EXTENDED_HISTORY (which makes it easier to find the spot where we left off reading the file after it gets re-written).
# By default, history movement commands visit the imported lines as well as the local lines, but you can toggle this on and off with the set-local-history zle binding. It is also possible to create a zle widget that will make some commands ignore imported commands, and some include them.
# If you find that you want more control over when commands get imported, you may wish to turn SHARE_HISTORY off, INC_APPEND_HISTORY or INC_APPEND_HISTORY_TIME (see above) on, and then manually import commands whenever you need them using ‘fc -RI’.


################################################################################
##    Prompting                                                               ##
################################################################################
setopt TRANSIENT_RPROMPT
# Remove any right prompt from display when accepting a command line. This may be useful with terminals with other cut/paste methods.


################################################################################
##    Input/Output                                                            ##
################################################################################
setopt NO_FLOW_CONTROL
# If this option is unset, output flow control via start/stop characters (usually assigned to ^S/^Q) is disabled in the shell’s editor.
