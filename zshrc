##           _
##   _______| |__  _ __ ___
##  |_  / __| '_ \| '__/ __|
##   / /\__ \ | | | | | (__
##  /___|___/_| |_|_|  \___|
##
##-- For login shell ----------------------------------------------------------+
##   zshenv  ->  zprofile  -> [zshrc] ->  zlogin
##-- For interactive shell ----------------------------------------------------+
##   zshenv  -> [zshrc]
##-----------------------------------------------------------------------------+
##

# Load zshrc.d/*
for i in $ZDOTDIR/zshrc.d/*; do
    . $i
done

# Load anyenv
export ANYENV_ROOT=/usr/local/opt/anyenv
export PATH=$ANYENV_ROOT/bin:$PATH
eval "$(anyenv init -)"

# Load zplug
export ZPLUG_HOME=/usr/local/opt/zplug
. $ZPLUG_HOME/init.zsh
zplug load

# Load fzf
. $ZPLUG_REPOS/junegunn/fzf/shell/completion.zsh
. $ZPLUG_REPOS/junegunn/fzf/shell/key-bindings.zsh

# Fxxk!!
eval $(thefuck --alias)

